﻿using sales.ViewModel;
using sales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace sales.Controllers
{
    public class HomeController : Controller
    {
        private SalesDbContext context;

        public HomeController()
        {
            if (context == null)
                context = new SalesDbContext();
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new IndexViewModel();
            return View();
        }

        [HttpPost]
        public ActionResult Index(string PromoCode)
        {
            if (context.Orders.FirstOrDefault(o => o.PromoCode.ToString() == PromoCode) != null)
                return RedirectToRoute(
                    new
                    {
                        controller = "Home",
                        action = "Order",
                        promoCode = PromoCode
                    }
                );
            else
            {
                var model = new IndexViewModel();
                model.PromoCode = PromoCode;
                ViewBag.Message = "Данный промо код не зарегистрирован!";
                return View(model);
            }


        }

        public ActionResult GetPromoCode()
        {
            Guid promoCode = Guid.NewGuid();
            Order Order = new Order() { PromoCode = promoCode, LastChange = DateTime.Now };
            context.Orders.Add(Order);
            context.SaveChanges();
            return RedirectToRoute(
                new
                {
                    controller = "Home",
                    action = "Order",
                    promoCode = promoCode
                }
                );

        }

        public ActionResult Order(Guid promoCode)
        {
            Order order = context.Orders.FirstOrDefault(o => o.PromoCode == promoCode);
            if (order != null)
            {
                if (!order.ready)
                {
                    var model = new SaleViewModel()
                    {
                        Order = order,
                        Books = context.Books.ToList()
                    };
                    return View(model);
                }
                else
                {
                    return RedirectToRoute(
                        new
                        {
                            controller = "Home",
                            action = "FinishingOrder",
                            promoCode = promoCode
                        }
                    );
                }
            }
            else
                return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult FinishingOrder(Guid promoCode)
        {

            Order order = context.Orders.FirstOrDefault(o => o.PromoCode == promoCode);
            if (order != null)
            {
                if (order.ready)
                {
                    return View(order);
                }
                else
                {
                    if (order.OrderSpecs.Sum(os => os.Book.Price) >= 2000)
                    {
                        order.ready = true;
                        context.SaveChanges();
                        return View(order);
                    }
                    else
                        return RedirectToRoute(
                            new
                            {
                                controller = "Home",
                                action = "Order",
                                promoCode = promoCode
                            }
                        );
                }
            }
            else
                return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult FinishingOrder([Bind(Include ="Promocode")] Order order)
        {
            order = context.Orders.FirstOrDefault(o => o.PromoCode == order.PromoCode);
            if (order != null && order.ready)
            {
                while (order.OrderSpecs.Count > 0)
                {
                    OrderSpec orderSpec = order.OrderSpecs.First();
                    context.OrderSpecs.Remove(orderSpec);
                }
                context.Orders.Remove(order);
                context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult CancelOrder(Guid promoCode)
        {
            Order order = context.Orders.FirstOrDefault(o => o.PromoCode == promoCode);
            if (order != null && order.ready)
            {
                while (order.OrderSpecs.Count > 0)
                {
                    OrderSpec orderSpec = order.OrderSpecs.First();
                    context.Books.First(b => b.Id == orderSpec.BookId).Count += 1;
                    context.OrderSpecs.Remove(orderSpec);
                }
                context.Orders.Remove(order);
                context.SaveChanges();
            }
            return RedirectToAction("Index");
        }



        public ActionResult GetBook(int Id, Guid promoCode)
        {
            Book book = context.Books.FirstOrDefault(b => b.Id == Id);
            Order order = context.Orders.FirstOrDefault(o => o.PromoCode == promoCode);
            if (book != null && order != null)
            {
                if (book.Count > 0)
                    if (order.OrderSpecs.FirstOrDefault(os => os.BookId == book.Id) == null)
                    {
                        OrderSpec orderSpec = new OrderSpec
                        {
                            Book = book,
                            Order = order
                        };
                        context.OrderSpecs.Add(orderSpec);
                        book.Count -= 1;
                        context.SaveChanges();
                    }

            }
            return RedirectToRoute(
                    new
                    {
                        controller = "Home",
                        action = "Order",
                        promoCode = promoCode
                    }
                    );
        }

        public ActionResult RemoveBook(int Id)
        {
            OrderSpec orderSpec = context.OrderSpecs.FirstOrDefault(os => os.Id == Id);

            if (orderSpec != null)
            {
                context.OrderSpecs.Remove(orderSpec);
                context.Books.First(b => b.Id == orderSpec.BookId).Count += 1;
                context.SaveChanges();

            }
            return RedirectToRoute(
                    new
                    {
                        controller = "Home",
                        action = "Order",
                        promoCode = orderSpec.OrderPromoCode
                    }
                    );
        }


    }
}