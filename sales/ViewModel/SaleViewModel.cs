﻿using sales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sales.ViewModel
{
    public class SaleViewModel
    {
        public Order Order { get; set; }

        public IEnumerable<Book> Books { get; set; }
    }
}