﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sales.Models
{
    public class Order
    {


        [Display(Name = "Промокод")]
        [Key]
        public Guid PromoCode { get; set; }

        [Display(Name = "Последнее изменение.")]
        public DateTime LastChange { get; set; }


        public bool ready { get; set; }

        public virtual ICollection<OrderSpec> OrderSpecs { get; set; }
    }

    public class OrderSpec
    {
        [Display(Name = "ID")]
        [Key]
        public int Id { get; set; }

        public Guid OrderPromoCode { get; set; }
        public virtual Order Order { get; set; }

        public int BookId { get; set; }
        public virtual Book Book { get; set; }
    }
}