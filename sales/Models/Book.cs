﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace sales.Models
{
    public class Book
    {
        [Display(Name = "ID")]
        [Key]
        public int Id { get; set; }

        [Display(Name = "Название книги")]
        [Required]
        public string Name{ get; set; }

        [Display(Name = "Автор книги")]
        [Required]
        public string Author{ get; set; }

        [Display(Name = "Год издания")]
        [Required]
        [Range(1000, 2100)]
        public int Year { get; set; }

        [Display(Name = "ISBN код")]
        [Required]
        public string ISBN { get; set; }

        public string URL { get; set; }

        [Display(Name = "Цена")]
        public Decimal Price { get; set; }

        [Display(Name = "Количество")]
        public int Count { get; set; }
    }
}