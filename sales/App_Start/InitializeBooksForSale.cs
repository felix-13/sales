﻿using sales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace sales.App_Start
{
    public static class InitializeBooksForSale
    {
        public static bool LoadBooksForSale()
        {
            using (SalesDbContext context = new SalesDbContext())
            {
                context.Books.RemoveRange(context.Books.ToList());
                context.OrderSpecs.RemoveRange(context.OrderSpecs.ToList());
                context.Orders.RemoveRange(context.Orders.ToList());
                context.SaveChanges();
            }

            try
            {
                
                XmlDocument doc = new XmlDocument();
                doc.Load(HttpContext.Current.Server.MapPath( "~/App_Data/BooksForSale.xml"));
                XmlElement root = doc.DocumentElement;
                if (root.Name == "Books")
                {
                    foreach (XmlNode rootChild in root.ChildNodes)
                    {
                        if (rootChild.Name == "Book")
                        {
                            Book book = new Book();
                            book.Name = rootChild.Attributes["Name"].Value;
                            book.Author = rootChild.Attributes["Author"].Value;
                            book.Year = int.Parse(rootChild.Attributes["Year"].Value);
                            book.ISBN = rootChild.Attributes["ISBN"].Value;
                            book.URL = rootChild.Attributes["PictureUrl"].Value;
                            book.Price = Decimal.Parse(rootChild.Attributes["Price"].Value);
                            book.Count = int.Parse(rootChild.Attributes["Count"].Value);
                            using (SalesDbContext context = new SalesDbContext()) 
                            {
                                context.Books.Add(book);
                                context.SaveChanges();
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}